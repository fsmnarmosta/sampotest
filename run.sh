#! /usr/bin/env bash

# Absolute path to the lib folder containing headers and shared object files

LIB_PATH=`pwd`/lib

# Absolute path to the header file

C_PATH=`pwd`/lib/sampo.h

# Forth command to add the libnoni.so -shared object file and link against the header in C_PATH

LIB_INCLUDE='s" sampo" add-lib \c #include "'"$C_PATH"'"'

# We need to pass LD_LIBRARY_PATH for LD to find the shared object file and LIBRARY_PATH for gcc to find the header file
# Then we execute the code in LIB_INCLUDE so that forth links and loads the right symbols

LD_LIBRARY_PATH="$LIB_PATH":"$LD_LIBRARY_PATH" LIBRARY_PATH="$LIB_PATH":"$LIBRARY_PATH" gforth -e "$LIB_INCLUDE" test.fs
