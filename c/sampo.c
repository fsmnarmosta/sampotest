#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>

SDL_Window *win = NULL;
SDL_Renderer *ren = NULL;

int *x_pos;
int *y_pos;
int *pen_state;
int stroke_container_size = 0;
int stroke_qty = 0;
int pen_is_down = 0;
int turtle_visible = 0;

void grow_stroke_container(){
	int *new_x_pos = (int*) malloc(sizeof(int)*stroke_container_size*2);
	int *new_y_pos = (int*) malloc(sizeof(int)*stroke_container_size*2);
	int *new_pen_state = (int*) malloc(sizeof(int)*stroke_container_size*2);
	stroke_container_size *= 2;
	for(int i = 0; i < stroke_qty; i++){
		new_x_pos[i] = x_pos[i];
		new_y_pos[i] = y_pos[i];
		new_pen_state[i] = pen_state[i];
	}
	free(x_pos);
	x_pos = new_x_pos;
	free(y_pos);
	y_pos = new_y_pos;
	free(pen_state);
	pen_state = new_pen_state;
}

int start(){
	int init_status = SDL_Init(SDL_INIT_VIDEO);
	if(init_status != 0)
		return init_status;
	win = SDL_CreateWindow("Sampo", 0, 0, 640, 480, SDL_WINDOW_SHOWN);
	if(win == NULL)
		return 1;
	ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (ren == NULL){
		return 1;
	}
	stroke_container_size = 1;
	grow_stroke_container();
	return 0;
}

void move_to(int x, int y){
	if(stroke_qty == stroke_container_size) grow_stroke_container();
	x_pos[stroke_qty] = x;
	y_pos[stroke_qty] = y;
	pen_state[stroke_qty] = pen_is_down;
	stroke_qty++;
}

void move(int x, int y){
	if(stroke_qty == 0){
		move_to(x, y);
	}else{
		move_to(x_pos[stroke_qty-1] + x, y_pos[stroke_qty-1]+y);
	}
}

void update_render(){
	SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
	SDL_RenderClear(ren);

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 1);
	for(int i = 0; i < stroke_qty-1; i++){
		if(pen_state[i+1])
			SDL_RenderDrawLine(ren, x_pos[i], y_pos[i], x_pos[i+1], y_pos[i+1]);
	}

	if(stroke_qty > 0 && turtle_visible){
		SDL_Rect rect;
		rect.x = x_pos[stroke_qty-1] - 4;
		rect.y = y_pos[stroke_qty-1] - 4;
		rect.w = 8;
		rect.h = 8;
		SDL_RenderFillRect(ren, &rect);
	}

	SDL_RenderPresent(ren);
}

void quit(){
	SDL_DestroyWindow(win);
	SDL_DestroyRenderer(ren);
	SDL_Quit();
	free(x_pos);
	free(y_pos);
	free(pen_state);
}

void wait_indefinately(){
        SDL_Event events;
	while(1) {
		SDL_PollEvent(&events);
		if(events.type == SDL_QUIT) break;
		SDL_Delay(10);
	}
	quit();
}

void penup(){
	pen_is_down = 0;
}

void pendown(){
	pen_is_down = 1;
}

void show_turtle(){
	turtle_visible = 1;
}

void hide_turtle(){
	turtle_visible = 0;
}
