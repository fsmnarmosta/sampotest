# Dependencies

```
sudo apt install gcc gforth libtool-bin libsdl2-dev
```

# Compiling

```
./build-c.sh
```

# Running

```
./run.sh
```
