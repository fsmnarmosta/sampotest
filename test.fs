#! /usr/bin/env gforth

( Define all the c functions )

c-function start-sampo start -- n
c-function quit-sampo quit -- void
c-function update-render update_render -- void
c-function move-to move_to n n -- void
c-function move-turtle move n n -- void
c-function wait-indefinately wait_indefinately -- void
c-function penup penup -- void
c-function pendown pendown -- void
c-function show-turtle show_turtle -- void
c-function hide-turtle hide_turtle -- void

( Draw sin 1/x  )

: draw-pattern
500 1 DO
  I dup
  1.0e s>f 0.001e f* f/ fsin 100.0e f* 100.0e f+ f>s
  move-to
  update-render
LOOP ;

( Draw a 10x10 square at the current location)

: draw-square
  pendown
  10 0 move-turtle
  update-render
  100 ms
  0 10 move-turtle
  update-render
  100 ms
  -10 0 move-turtle
  update-render
  100 ms
  0 -10 move-turtle
  update-render
  100 ms
  penup ;

( Draw a row of 10x10 squares )

: draw-squares
  10 0 DO
  20 0 move-turtle
  draw-square
  100 ms
LOOP ;

( Starting debug prints )

." Starting sampo" cr
start-sampo .
." Sampo successfully up" cr

( Demo code )

( Display the turtle )

show-turtle

( Draw a set of squares )

draw-squares

( Draw another set of squares, relative to (100, 100\)  )

100 100 move-to
draw-squares

( Draw a topologist's sine curve )

0 0 move-to
pendown
draw-pattern
hide-turtle
update-render

( Wait until the window is closed )

wait-indefinately

( End debug print )

." Sampo successfully down" cr
bye
